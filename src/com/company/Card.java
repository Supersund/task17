package com.company;

public abstract class Card implements Money {
    protected double balance;
    private String name;

    public Card(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return this.name;
    }
}
