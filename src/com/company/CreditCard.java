package com.company;

public class CreditCard extends Card {
    private double creditLimit;
    private double creditUsed;

    @Override
    public boolean spend(double amount) {
        if (!canAfford(amount)) return false;
        this.balance -= amount;
        if (this.balance < 0) {
            this.creditUsed -= this.balance;
            this.balance = 0;
        }
        return true;
    }

    @Override
    public boolean canAfford(double amount) {
        return (creditLimit - creditUsed + balance) >= amount;
    }

    public CreditCard(String name) {
        super(name);
        this.creditLimit = 0;
        this.balance = 0;
        this.creditUsed = 0;
    }


    @Override
    public double getBalance() {
        return this.balance;
    }

    public double getCreditLimit() {
        return this.creditLimit;
    }


    public boolean trySetCreditLimit(double limit) {
        if (limit < 0) {
            return false;
        }
        if (limit < this.creditUsed) {
            return false;
        }
        this.creditLimit = limit;
        return true;
    }


    public double getCreditUsed() {
        return creditUsed;
    }

    @Override
    public void add(double amount) {
        if (this.creditUsed == 0) {
            this.balance += amount;
        } else {
            this.creditUsed -= amount;
            if (this.creditUsed < 0) {
                this.balance -= this.creditUsed;
                this.creditUsed = 0;
            }
        }
    }
}
