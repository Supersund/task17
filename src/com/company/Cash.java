package com.company;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Cash implements Money {

    private String name;
    private Map<Double, Integer> wallet;
    final private double[] denominations = {500.00, 100.00, 20.00, 5.00, 1.00, 0.25, 0.05, 0.01};

    public Cash(String name) {
        this.name = name;
        this.wallet = new HashMap<Double, Integer>();
        this.wallet.put(0.01, 0);
        this.wallet.put(0.05, 0);
        this.wallet.put(0.25, 0);
        this.wallet.put(1.00, 0);
        this.wallet.put(5.00, 0);
        this.wallet.put(20.00, 0);
        this.wallet.put(100.00, 0);
        this.wallet.put(500.00, 0);
    }

    private void setWalletToZero() {
        for (double key: denominations) {
            wallet.put(key, 0);
        }
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public boolean spend(double amount) {
        if (!canAfford(amount)) return false;
        double change = this.getBalance()-amount;
        this.setWalletToZero();
        this.add(change);
        return true;
    }

    @Override
    public boolean canAfford(double amount) {
        return this.getBalance() >= amount;
    }

    @Override
    public void add(double amount) {
        for (double key : this.denominations) {
            if (amount > key) {
                wallet.put(key, wallet.get(key)+(int)(amount/key));
                amount -= key*(int)(amount/key);
            }
        }
    }

    @Override
    public double getBalance() {
        double balance = 0;
        Iterator hmItarator = wallet.entrySet().iterator();
        while (hmItarator.hasNext()) {
            Map.Entry element = (Map.Entry) hmItarator.next();
            double denomination = (double)element.getKey();
            int amount = (int)element.getValue();
            balance += denomination*amount;
        }
        return balance;
    }

    @Override
    public String toString() {
        return "Cash{" +
                "wallet=" + wallet +
                " Total balance: "+this.getBalance()+"}";
    }
}
