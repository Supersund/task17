package com.company;

public class DebitCard extends Card {

    @Override
    public double getBalance() {
        return this.balance;
    }

    @Override
    public boolean spend(double amount) {
        if (!this.canAfford(amount)) return false;
        this.balance -= amount;
        return true;
    }

    @Override
    public boolean canAfford(double amount) {
        return this.balance >= amount;
    }

    @Override
    public void add(double amount) {
        this.balance += amount;
    }

    public DebitCard(String name) {
        super(name);
        this.balance = 0;
    }
}
