package com.company;

public interface Money {
    double getBalance();
    void add(double amount);
    boolean canAfford(double amount);
    boolean spend(double amount);
    String getName();
}
