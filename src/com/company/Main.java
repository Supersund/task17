package com.company;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;

public class Main {
    private HashMap<String, Money> accounts = new HashMap<>();
    private Scanner scanner;


    public static void main(String[] args) {
        Main main = new Main();
    }

    private Main() {
        this.scanner = new Scanner(System.in);
        System.out.println("Welcome to this banking system");
        this.mainOptions();
    }

    private void printExistingAccounts() {
        Iterator iterator = accounts.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry element = (Map.Entry) iterator.next();
            System.out.println(element.getKey());
        }
    }

    private void mainOptions() {
        System.out.println("Choose an account. If your account name does not match any existing account a new one will be created");
        System.out.println("Current accounts:");
        printExistingAccounts();
        System.out.print("Enter account name: ");
        String accountName = this.scanner.nextLine();
        if (!accounts.containsKey(accountName)) createAccount(accountName);
        else getAccountOptions(accountName);
    }

    private void createAccount(String accountName) {
        System.out.println("Welcome to account creating! You are now creating an account with the name: " + accountName);
        System.out.println("What kind of account would you like to make?");
        System.out.println("Press 1 for cash (Weird to make a cash account, but who cares, Craig?");
        System.out.println("Press 2 for debit account");
        System.out.println("Press 3 for credit account");
        System.out.print("Choose type: ");
        String accountType = this.scanner.nextLine();
        if (accountType.equals("1")) {
            System.out.println("GREEEAT, you have created a new cash account!");
            accounts.put(accountName, new Cash(accountName));
            mainOptions();
        } else if (accountType.equals("2")) {
            System.out.println("Nice, pal! You have created a debit account");
            accounts.put(accountName, new DebitCard(accountName));
            mainOptions();
        } else if (accountType.equals("3")) {
            System.out.println("Howdy fellah, spending them money you don't have, eh? Well, gratz with a new credit account");
            accounts.put(accountName, new CreditCard(accountName));
            mainOptions();
        } else createAccount(accountName);
    }

    private void getAccountOptions(String accountName) {
        Class accountClass = accounts.get(accountName).getClass();
        if (accountClass == Cash.class) {
            cashOptions((Cash) (accounts.get(accountName)));
        } else if (accountClass == CreditCard.class) {
            creditOptions((CreditCard) (accounts.get(accountName)));
        } else if (accountClass == DebitCard.class) {
            debitOptions((DebitCard) (accounts.get(accountName)));
        } else {
            System.out.println("This was a weird error");
            mainOptions();
        }
    }

    private void cashOptions(Cash cashAccount) {
        System.out.println("Welcome to your cash account. Current account balance: " + cashAccount.getBalance());
        System.out.println("What do you want to do?");
        System.out.println("Press 1 to add money");
        System.out.println("Press 2 to spend money");
        System.out.println("Press 3 to see the coins in your wallet. Weirdo.");
        System.out.print("Your choice: ");
        String choice = this.scanner.nextLine();
        if (choice.equals("1")) this.addMoney(cashAccount);
        else if (choice.equals("2")) this.spendMoney(cashAccount);
        else if (choice.equals("3")) {
            System.out.println(cashAccount.toString());
            mainOptions();
        }
        else cashOptions(cashAccount);
    }

    private void debitOptions(DebitCard debitCard) {
        System.out.println("Welcome to your debit account. Current account balance: " + debitCard.getBalance());
        System.out.println("What do you want to do?");
        System.out.println("Press 1 to add money");
        System.out.println("Press 2 to spend money");
        String choice = this.scanner.nextLine();
        if (choice.equals("1")) addMoney(debitCard);
        else if (choice.equals("2")) spendMoney(debitCard);
        else debitOptions(debitCard);

    }

    private void creditOptions(CreditCard creditCard) {
        System.out.println("Welcome to your credit account. Current account balance: " + creditCard.getBalance());
        System.out.println("Your current credit limit is: " + creditCard.getCreditLimit());
        System.out.println("Your currently used credit is: " + creditCard.getCreditUsed());
        System.out.println("What do you want to do?");
        System.out.println("Press 1 to add money");
        System.out.println("Press 2 to spend money");
        System.out.println("Press 3 to change credit limit");
        System.out.print("Your choice: ");
        String choice = this.scanner.nextLine();
        if (choice.equals("1")) addMoney(creditCard);
        else if (choice.equals("2")) spendMoney(creditCard);
        else if (choice.equals("3")) changeCreditLimit(creditCard);
        else creditOptions(creditCard);

    }

    private void addMoney(Money account) {
        System.out.println("How much money do you want to add?");
        System.out.print("Amount: ");
        double amount = 0;
        try {
            amount = Double.parseDouble(this.scanner.nextLine());
        } catch (Exception e) {
            System.out.println("Write a number, for God's sake!");
            addMoney(account);
            return;
        }
        if (amount <= 0) {
            System.out.println("Your amount was below 0. Really? Is it that hard? Try again!");
            addMoney(account);
        }
        account.add(amount);
        System.out.println("Money was added to your account! Lucky dog!");
        mainOptions();

    }

    private void spendMoney(Money account) {
        System.out.println("How much money do you want to spend?");
        System.out.print("Amount: ");
        double amount = 0;
        try {
            amount = Double.parseDouble(this.scanner.nextLine());
        } catch (Exception e) {
            System.out.println("You have to write a number, son!");
            spendMoney(account);
            return;
        }
        if (amount <= 0) {
            System.out.println("Your amount was 0 or below. Try again!");
            spendMoney(account);
            return;
        }
        if (account.spend(amount)) {
            System.out.println("Money well spent!");
        } else {
            System.out.println("Sorry, you don't have enough money! Do the smart thing; add more money!");
        }
        mainOptions();
    }

    private void changeCreditLimit(CreditCard creditCard) {
        System.out.println("Welcome to the creditlimitsetter! Your current limit is: " + creditCard.getCreditLimit());
        System.out.print("New limit: ");
        double newLimit = 0;

        try {
            newLimit = Double.parseDouble(scanner.nextLine());
        } catch (Exception e) {
            System.out.println("You gotta try to add a number, Craig!");
            changeCreditLimit(creditCard);
            return;
        }
        if (creditCard.trySetCreditLimit(newLimit)) {
            System.out.println("Good job! New limit is set!");
        } else {
            System.out.println("Sorry, dude! Cannot set that limit!");
        }
        mainOptions();

    }
}
